/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>


#define VC_ASOC 16
using namespace std;

class VCcache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST1: victim cache hit
 * 1. Choose a random associativity for L1
 * 2. Fill l1 and vc cache entries
 * 3. Force a miss on L1 and a hit on VC
 * 4. Check  miss_hit_status for VC
 */
TEST_F(VCcache,l1_miss_vc_hit){
	//srand (time(NULL)); 
	int status = OK;
	int i,k;
  	int idx;
  	int tag;
	
	bool match = false;
	int associativity_l1;

	

	enum miss_hit_status expected_miss_hit;
	struct l1_vc_entry_info l1_vc_info;
   operation_result l1_result = {};
   operation_result vc_result = {};
	/*Fill a random cache entry*/
	idx = rand()%1024;
	tag = rand()%4096;
	associativity_l1 = 1 << (rand()%4);
	struct entry l1_cache_blocks[associativity_l1] = {};
	struct entry vc_cache_blocks[VC_ASOC] = {};
	l1_vc_info.l1_tag=tag;
	l1_vc_info.l1_associativity=associativity_l1;
	l1_vc_info.vc_associativity=VC_ASOC;
	l1_vc_info.l1_idx = idx;
if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity_l1);

  }

	DEBUG(debug_on, l1_miss_vc_hit_test);
	for (i = 0 ; i < 2; i++)
	{
    /* Fill l1 */
		for ( i =  0; i < associativity_l1; i++) {
			
			l1_cache_blocks[i].valid = true;
			l1_cache_blocks[i].tag = rand()%4096;
			l1_cache_blocks[i].dirty = 0;
			l1_cache_blocks[i].rp_value = i;
			//forzar miss en l1
			while (l1_cache_blocks[i].tag == tag) {
				l1_cache_blocks[i].tag = rand()%4096;
			}
			
		}
		/*fill vc*/
		int victim_rp_before[VC_ASOC]; 

		for(k =0; k <VC_ASOC; k++)
		{
		
			vc_cache_blocks[k].valid = true;
			vc_cache_blocks[k].tag = rand()%4096;	
			vc_cache_blocks[k].dirty = 0;
			vc_cache_blocks[k].rp_value = k;
			 victim_rp_before[k] = vc_cache_blocks[k].rp_value; // se llena un arreglo para verificar que el rp se modifique de forma correcta
			
		}
			//Forzar un hit en vc
			vc_cache_blocks[3].tag = tag; // se fuerza hit en 3 para verificar que los rp values menores a 3 se modifican		
		
		if(bool (debug_on))
		{
			cout << "INFO" <<endl;
		 	print_set(l1_cache_blocks,associativity_l1,""); 
			print_set(vc_cache_blocks,VC_ASOC,""); 
		}
		bool loadstore = (bool)i;
		match = false;
		
  		status = lru_replacement_policy_l1_vc(&l1_vc_info,
  		    	                        		loadstore,
  		      	                      	      	l1_cache_blocks,
  		        	                    	    vc_cache_blocks,
  		          	                  	      	&l1_result,
  		            	                	    &vc_result, false);
		
		int victim_rp_after[VC_ASOC];
		
		

		EXPECT_EQ(status, OK);
		EXPECT_EQ(vc_result.dirty_eviction, 0);
    	expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    	EXPECT_EQ(vc_result.miss_hit, expected_miss_hit);
		///Se agregan test de rp value
		EXPECT_EQ(victim_rp_before[3], 3);
		for(k =0; k <VC_ASOC; k++)
		{
			victim_rp_after[k] = vc_cache_blocks[k].rp_value;
			if( k<3)
			{
				EXPECT_EQ(victim_rp_after[k], k+1); //todos los rp menores al del hit se aumentan en 1
			}
			else if (k == 3)
			{
				EXPECT_EQ(victim_rp_after[k], 0); //el rp del hit se coloca en cero
			}
			else
			{
				EXPECT_EQ(victim_rp_after[k], k); // el rp de los valores mayores al del hit quedan igual	
			}
			
			
		}
		

		

	}


  
}



/*
 * TEST2: victim cache miss
 * 1. Choose a random associativity for L1
 * 2. Fill l1 and vc cache entries
 * 3. Force a miss on L1 and a miss on VC
 * 4. Check  miss_hit_status for VC
 */
TEST_F(VCcache,l1_miss_vc_miss){
	
	int status = OK;
	int i,k;
  	int idx;
  	int tag;
	bool match = false;
	int associativity_l1;
	enum miss_hit_status expected_miss_hit;
	struct l1_vc_entry_info l1_vc_info;
   	operation_result l1_result = {};
   	operation_result vc_result = {};
	/*Fill a random cache entry*/
	idx = rand()%1024;
	tag = rand()%4096;
	associativity_l1 = 1 << (rand()%4);
	struct entry l1_cache_blocks[associativity_l1] = {};
	struct entry vc_cache_blocks[VC_ASOC] = {};
	l1_vc_info.l1_tag=tag;
	l1_vc_info.l1_associativity=associativity_l1;
	l1_vc_info.vc_associativity=VC_ASOC;
	l1_vc_info.l1_idx = idx;
if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity_l1);
  }

	DEBUG(debug_on, l1_miss_vc_miss_test);
	for (i = 0 ; i < 2; i++)
	{
    /* Fill l1 */
		for ( i =  0; i < associativity_l1; i++) 
		{
				
				l1_cache_blocks[i].valid = true;
				l1_cache_blocks[i].tag = rand()%4096;
				l1_cache_blocks[i].dirty = 0;
				l1_cache_blocks[i].rp_value = i;
				//forzar miss en l1
				while (l1_cache_blocks[i].tag == tag) 
				{
					l1_cache_blocks[i].tag = rand()%4096;
				}
		}
		/*fill vc*/
		int victim_rp_before[VC_ASOC]; 
		for(k =0; k <VC_ASOC; k++)
		{
			vc_cache_blocks[k].valid = true;
			//Forzar miss en vc
			while( vc_cache_blocks[k].tag == tag)
			{
				vc_cache_blocks[k].tag = rand()%4096;
			}
			vc_cache_blocks[k].dirty = 0;
			vc_cache_blocks[k].rp_value = k;
			victim_rp_before[k] =vc_cache_blocks[k].rp_value;
		}

		
		bool loadstore = (bool)i;	
		if(bool (debug_on))
		{
			cout << "INFO" <<endl;
			print_set(l1_cache_blocks,associativity_l1,""); 
			print_set(vc_cache_blocks,VC_ASOC,""); 
		}
  		status = lru_replacement_policy_l1_vc(&l1_vc_info,
  		    	                        		loadstore,
  		      	                      	      	l1_cache_blocks,
  		        	                    	    vc_cache_blocks,
  		          	                  	      	&l1_result,
  		            	                	    &vc_result, false);
								
		int victim_rp_after[VC_ASOC];
											
		EXPECT_EQ(status, OK);
		EXPECT_EQ(vc_result.dirty_eviction, 0); 
    	expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    	EXPECT_EQ(vc_result.miss_hit, expected_miss_hit);
		//Se agregan test para comprobar el comportamiento del rp value
		for(k =0; k <VC_ASOC; k++)
		{
			victim_rp_after[k] = vc_cache_blocks[k].rp_value;
			if( k<VC_ASOC-1)
			{
				EXPECT_EQ(victim_rp_after[k], k+1); //todos los rp menores al ultimo rp se aumentan en 1
			}
			else
			{
				EXPECT_EQ(victim_rp_after[k], 0); //el rp del ultimo elemento pasa a cero debido a la victimizacion
			}
			
				
			
			
			
			
		}
	} 
}


/*
 * TEST3: vc_tag_bug
 * 1. Re-build VC tag
 * 2. Force a hit with the used tag (L1 cache tag)
 * 3 Displace the used tag, an amount of l1 idx size to match VC real tag size 
 * 4. Check  if the used tag matches with real_tag
 */
TEST_F(VCcache,vc_tag_bug){
	
	int status = OK;
	int i,k;
  	int idx;
  	int tag;
	int vc_real_tag;
	bool match = false;
	int associativity_l1;
	enum miss_hit_status expected_miss_hit;
	struct l1_vc_entry_info l1_vc_info;
   	operation_result l1_result = {};
   	operation_result vc_result = {};
	/*Fill a random cache entry*/
	idx = rand()%40+1; //Se restringe a un idx pequeno para mostrar que puede pasar el test (con 2,4,30 pasa)
	tag = rand()%4096;
	associativity_l1 = 1 << (rand()%4);
	struct entry l1_cache_blocks[associativity_l1] = {};
	struct entry vc_cache_blocks[VC_ASOC] = {};
	l1_vc_info.l1_tag=tag;
	l1_vc_info.l1_associativity=associativity_l1;
	l1_vc_info.vc_associativity=VC_ASOC;
	l1_vc_info.l1_idx = idx;
	/////Se encuentra el tamano del idx/////	
	int idx_copy = idx;
	int idx_size=0;
	int bits_number = 1;
	int counter = 0;
	cout <<  idx  << endl;

	while(idx_copy != 1){
	
    	idx_copy = idx_copy >> 1;
    	idx_size ++;
		bits_number = bits_number*2;
	
   }
   if( idx > bits_number )
   {
	   idx_size = idx_size +1;
   }
   
	cout << bits_number<<" "<<  idx <<" " <<idx_size << endl;
//////////////////////////////////////////
	vc_real_tag = ((tag<<idx_size) | idx); //se construye manualmente el idx del vc, concatenando los bits del idx al tag 
if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity_l1);
  }

	DEBUG(debug_on, l1_miss_vc_miss_test);
	for (i = 0 ; i < 2; i++)
	{
    /* Fill l1 */
		for ( i =  0; i < associativity_l1; i++) 
		{
				
				l1_cache_blocks[i].valid = true;
				l1_cache_blocks[i].tag = rand()%4096;
				l1_cache_blocks[i].dirty = 0;
				l1_cache_blocks[i].rp_value = i;
				//forzar miss en l1
				while (l1_cache_blocks[i].tag == tag) 
				{
					l1_cache_blocks[i].tag = rand()%4096;
				}
		}
		/*fill vc*/
		for(k =0; k <VC_ASOC; k++)
		{
			vc_cache_blocks[k].valid = true;
			vc_cache_blocks[k].tag = rand()%4096;
			vc_cache_blocks[k].dirty = 0;
			vc_cache_blocks[k].rp_value = k;
		}
			vc_cache_blocks[0].tag = tag;

			int used_tag = tag<< idx_size;
			bool loadstore = (bool)i;	
			if(bool (debug_on))
			{
				cout << "INFO" <<endl;
		 		print_set(l1_cache_blocks,associativity_l1,""); 
				print_set(vc_cache_blocks,VC_ASOC,""); 
			}
  			status = lru_replacement_policy_l1_vc(&l1_vc_info,
  			    	                        		loadstore,
  			      	                      	      	l1_cache_blocks,
  			        	                    	    vc_cache_blocks,
  			          	                  	      	&l1_result,
  			            	                	    &vc_result, false);

			EXPECT_EQ(status, OK);
			EXPECT_EQ(vc_real_tag, used_tag); //Si el tag construido es distinto al tag con el que se indexa( el de l1) el test falla
	} 
}


