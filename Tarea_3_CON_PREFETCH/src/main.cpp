#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <L2cache.h>
#include <debug_utilities.h>
#include <time.h>

using namespace std;

/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

#define BUFFER_SIZE 400

int main(int argc, char * argv []) {
  clock_t start = clock();


  configuration conf = {};
  for (size_t i = 1; i < argc; i++) {
      if (strcmp(argv[i],"-t")==0){
          conf.cache_size_L1 = atoi(argv[i+1]);
      }else if (strcmp(argv[i],"-l")==0){
          conf.block_size = atoi(argv[i+1]);
      }else if (strcmp(argv[i],"-a")==0){
          conf.associativity_L1 = atoi(argv[i+1]);
      }else if (strcmp(argv[i],"-opt")==0){
          if(strcmp(argv[i+1],"l2")==0)
            conf.opt_policy = 0;//0 for multilevel
          else if(strcmp(argv[i+1],"vc")==0)
            conf.opt_policy = 1;//0 for victim

      }
  }
  conf.cache_size_L2 = 4*conf.cache_size_L1;
  conf.associativity_L2= 2*conf.associativity_L1;


  stats sim_stats = simulate(stdin,conf);

  print_configuration(conf);

  print_stats(sim_stats, conf);


  printf("Tiempo transcurrido: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}






/*
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char * argv []) {
  address_data* address_data; 



  
long address = 0x7fffed80;

  printf("Do something :), don't forget to keep track of execution time\n");
address_tag_idx_get( address, address_data);
return 0;
}
*/