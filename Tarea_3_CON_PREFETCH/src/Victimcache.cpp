/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>
#include <iostream> 
#include <vector> 

#define KB 1024
#define ADDRSIZE 32
#define LRU_WT 1
#define LRU_WB 2
using namespace std;

int lru_replacement_policy_l1_vc(const l1_vc_entry_info *l1_vc_info,
      	                      	 	bool loadstore,
        	                    	entry* l1_cache_blocks,
          	                  	 	entry* vc_cache_blocks,
            	                	operation_result* l1_result,
              	              	 	operation_result* vc_result,
                	            	bool debug)
{
	if(l1_vc_info->l1_idx < 0 || l1_vc_info->l1_tag < 0 || l1_vc_info->l1_associativity < 1|| l1_vc_info->vc_associativity<1){
    	return PARAM;
   }
   	if(debug)printf("\n[access: %i]\n", l1_vc_info->l1_tag);
   	int way_free      = -1;
   	int candidate_vic = -1;
	int candidate_vic_vc = -1;   
   	int way_free_vc = -1;
	bool hit_vc = false;
	
	
   for(size_t i = 0; i < l1_vc_info->l1_associativity; i++)
   {
		if(l1_cache_blocks[i].valid == 1)
		{	
			
			if(l1_cache_blocks[i].tag == l1_vc_info->l1_tag)
			{		
				l1_result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
				int threshold = l1_cache_blocks[i].rp_value;
				update_entry(&l1_cache_blocks[i], l1_vc_info->l1_tag,loadstore,-1,LRU_WB);
				update_rp_value_lru(l1_cache_blocks,l1_vc_info->l1_associativity,threshold);
				if(debug)
				{
					print_set(l1_cache_blocks,l1_vc_info->l1_associativity, "");
					print_result(*l1_result,"l1_result : ");
				}
				return OK; 
			}
			if(l1_cache_blocks[i].rp_value == l1_vc_info->l1_associativity-1)
			{
				candidate_vic = i;
			}
		}
		else
		{
			way_free = i;
		}
   }

   
   if(way_free != -1)
   {
	   ////implementacion vc 1//////
		for(size_t i = 0; i < l1_vc_info->vc_associativity; i++)
		{
			if(vc_cache_blocks[i].valid == 1)
			{ 
				if(vc_cache_blocks[i].tag == l1_vc_info->l1_tag)
				{
					hit_vc = true;
					vc_result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
					int aux_tag = l1_cache_blocks[way_free].tag;
					int threshold_vc = vc_cache_blocks[i].rp_value;
					int threshold_l1 = l1_cache_blocks[way_free].rp_value;
					//L1
					update_entry(&l1_cache_blocks[way_free],vc_cache_blocks[i].tag,loadstore,-1,LRU_WB);
      				update_rp_value_lru(l1_cache_blocks,l1_vc_info->l1_associativity,threshold_l1);
					
					//vc
					update_entry_vc(&vc_cache_blocks[i],l1_vc_info->l1_tag,loadstore,-1,0); 
					update_rp_value_lru(vc_cache_blocks,l1_vc_info->vc_associativity,threshold_vc);
					
					
					
					if(debug)
				{
					print_set(l1_cache_blocks,l1_vc_info->l1_associativity, "");
					print_result(*l1_result,"l1_result : ");
					print_set(vc_cache_blocks,l1_vc_info->vc_associativity, "");
					print_result(*vc_result,"vc_result : ");
				}
					return OK;
					
				}
			
			}
		}	
				vc_result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
				update_entry(&l1_cache_blocks[way_free], l1_vc_info->l1_tag,loadstore,-1,LRU_WB);
    			update_rp_value_lru(l1_cache_blocks,l1_vc_info->l1_associativity,l1_vc_info->l1_associativity-1);
    			if(debug)
				{
        			print_set(l1_cache_blocks,l1_vc_info->l1_associativity, "");
        			print_result(*l1_result,"l1_result : ");
    			}
    			return OK;

			  	
   }
   else
   {
    	l1_result->evicted_address = l1_cache_blocks[candidate_vic].tag;
      	l1_result->dirty_eviction  = l1_cache_blocks[candidate_vic].dirty;
      	if(debug)
		{ 
        	print_entry(l1_cache_blocks[candidate_vic],"victimized entry: ");
        	printf("\n");
    	}
      	int threshold = l1_cache_blocks[candidate_vic].rp_value;
		
		
		///implementacion vc 2////
		for(size_t k = 0; k < l1_vc_info->vc_associativity; k++)
		{
			if(vc_cache_blocks[k].valid == 1)
			{	
				if(vc_cache_blocks[k].tag == l1_vc_info->l1_tag)
				{	
					
					vc_result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
					int aux_tag = l1_cache_blocks[candidate_vic].tag;
					int threshold_vc = vc_cache_blocks[k].rp_value;
					//l1
					update_entry(&l1_cache_blocks[candidate_vic],vc_cache_blocks[k].tag,loadstore,-1,LRU_WB); //revisar el dirty eviction
      				update_rp_value_lru(l1_cache_blocks,l1_vc_info->l1_associativity,threshold);
					//vc
					update_entry(&vc_cache_blocks[k],aux_tag,loadstore,-1,LRU_WB); 
					update_rp_value_lru(vc_cache_blocks,l1_vc_info->vc_associativity,threshold_vc);
					
					if(debug)
					{
						print_set(l1_cache_blocks,l1_vc_info->l1_associativity, "");
						print_result(*l1_result,"l1_result : ");
						print_set(vc_cache_blocks,l1_vc_info->vc_associativity, "");
						print_result(*vc_result,"l1_result : ");
					}
					
					return OK;
					
				}
				if(vc_cache_blocks[k].rp_value == l1_vc_info->vc_associativity-1)
				{
					
            		candidate_vic_vc = k;
         		}
			}
			else
			{
				
				way_free_vc = k;		
			}
		}			
			vc_result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
			if( way_free_vc != -1)
			{
					
				//victim
				if(vc_cache_blocks[way_free_vc].rp_value ==0 && vc_cache_blocks[way_free_vc].tag != 0 )
				{
					update_entry(&vc_cache_blocks[way_free_vc],l1_cache_blocks[candidate_vic].tag,loadstore,0,LRU_WB);
				}
				else
				{
					update_entry(&vc_cache_blocks[way_free_vc],l1_cache_blocks[candidate_vic].tag,loadstore,-1,LRU_WB); 
					update_rp_value_lru(vc_cache_blocks,l1_vc_info->vc_associativity,l1_vc_info->vc_associativity-1);	 
				}
			
				//l1cache
				update_entry(&l1_cache_blocks[candidate_vic],l1_vc_info->l1_tag,loadstore,-1,LRU_WB);
				update_rp_value_lru(l1_cache_blocks,l1_vc_info->l1_associativity,l1_vc_info->l1_associativity-1);
				
      		
      			if(debug)
				{
					print_set(l1_cache_blocks,l1_vc_info->l1_associativity, "");
					print_result(*l1_result,"l1_result : ");
					print_set(vc_cache_blocks,l1_vc_info->vc_associativity, "");
					print_result(*vc_result,"l1_result : ");
				}
				
      			return OK;
			}
			 else
			{
				
				vc_result->evicted_address = vc_cache_blocks[candidate_vic_vc].tag;
      			vc_result->dirty_eviction  = vc_cache_blocks[candidate_vic_vc].dirty;
				  
				
				//victim
				update_entry(&vc_cache_blocks[candidate_vic_vc],l1_cache_blocks[candidate_vic].tag,loadstore,-1,LRU_WB); 
				update_rp_value_lru(vc_cache_blocks,l1_vc_info->vc_associativity,l1_vc_info->vc_associativity-1);
				
				//l1cache
				update_entry(&l1_cache_blocks[candidate_vic],l1_vc_info->l1_tag,loadstore,-1,LRU_WB);
      			update_rp_value_lru(l1_cache_blocks,l1_vc_info->l1_associativity,l1_vc_info->l1_associativity-1);
      			if(debug)
				{
					print_set(l1_cache_blocks,l1_vc_info->l1_associativity, "");
					print_result(*l1_result,"l1_result : ");
					print_set(vc_cache_blocks,l1_vc_info->vc_associativity, "");
					print_result(*vc_result,"vc_result : ");
				}
				return OK;
					
			}		
   }
   return ERROR;
}



void update_entry_vc(entry* entry, int tag,int loadstore, int rp_value,int vc_valid){
   
    entry->dirty = true;
   

   	if(vc_valid == 1)
   	{
   	entry->valid    = true;
   	}
   	else if(vc_valid == 0)
   	{ 
		entry->valid    = 0;
	}
   entry->tag      = tag;
   entry->rp_value = rp_value; 
}



