/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
#define LRU_WT 1
#define LRU_WB 2
using namespace std;

int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks, 
                             operation_result* result,
                             bool debug)
{
 return OK;  
}


int lru_replacement_policy_l1_l2(const l1_l2_entry_info *l1_l2_entry_info,
																 bool loadstore,
																 entry* l1_cache_blocks,
																 entry* l2_cache_blocks,
																 operation_result* l1_result,
																 operation_result* l2_result,
																 bool debug) 
{
	if( l1_l2_entry_info->l1_tag < 0 || l1_l2_entry_info->l1_associativity< 1 ||  l1_l2_entry_info->l2_tag < 0 || l1_l2_entry_info->l2_associativity< 1){
		return PARAM;
	}
	if(debug)printf("\n[access: %i]\n", l1_l2_entry_info->l1_tag);
	int way_free      = -1;
	int candidate_vic = -1;
	bool write_on_l1=false;
	l1_result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;

   //FOR L1
   for(size_t i = 0; i < l1_l2_entry_info->l1_associativity; i++){
      if(l1_cache_blocks[i].valid == 1){
         if(l1_cache_blocks[i].tag ==l1_l2_entry_info->l1_tag){
			l1_result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
            int threshold = l1_cache_blocks[i].rp_value;
            update_entry(&l1_cache_blocks[i], l1_l2_entry_info->l1_tag,loadstore,-1, LRU_WT);
            update_rp_value_lru(l1_cache_blocks,l1_l2_entry_info->l1_associativity,threshold);
            if(debug){
				
            	print_set(l1_cache_blocks,l1_l2_entry_info->l1_associativity,"L1 Cache");
               	print_result(*l1_result,"result : ");
            }
				
			break;
         }
         if(l1_cache_blocks[i].rp_value == l1_l2_entry_info->l1_associativity-1){
            candidate_vic = i;
         }
      }else{
         way_free = i;
      }
   }

	if(way_free != -1){
		write_on_l1=true;
		update_entry(&l1_cache_blocks[way_free], l1_l2_entry_info->l1_tag,loadstore,-1,LRU_WT);
		update_rp_value_lru(l1_cache_blocks,l1_l2_entry_info->l1_associativity,l1_l2_entry_info->l1_associativity-1);
		if(debug){
			print_set(l1_cache_blocks,l1_l2_entry_info->l1_associativity,"L1 Cache");
			print_result(*l1_result,"result : ");
		}

   }else{
		write_on_l1=true;
		if(debug){ 
			print_entry(l1_cache_blocks[candidate_vic],"Victimized entry: ");
			printf("\n");
		}
		int threshold = l1_cache_blocks[candidate_vic].rp_value;
		update_entry(&l1_cache_blocks[candidate_vic],l1_l2_entry_info->l1_tag,loadstore,-1,LRU_WT);
		update_rp_value_lru(l1_cache_blocks,l1_l2_entry_info->l1_associativity,threshold);
		if(debug){
			print_set(l1_cache_blocks,l1_l2_entry_info->l1_associativity,"L1 Cache");
			print_result(*l1_result,"result : ");
		}
	}

	//FOR L2 
   	way_free      = -1;
	candidate_vic = -1;
	l2_result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
	for(size_t i = 0; i < l1_l2_entry_info->l2_associativity ; i++){
    	if(l2_cache_blocks[i].valid == 1){
			if(l2_cache_blocks[i].tag == l1_l2_entry_info->l2_tag){
				l2_result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
				int threshold = l2_cache_blocks[i].rp_value;
				update_entry(&l2_cache_blocks[i], l1_l2_entry_info->l2_tag,loadstore,-1,LRU_WB);
				update_rp_value_lru(l2_cache_blocks,l1_l2_entry_info->l2_associativity,threshold);
				if(debug){
				print_set(l2_cache_blocks,l1_l2_entry_info->l2_associativity,"L2 cache");
				print_result(*l2_result,"result : ");
				}
			}
			if(l2_cache_blocks[i].rp_value == l1_l2_entry_info->l2_associativity-1){
				candidate_vic = i;
			}
    	}else{
         way_free = i;
      	}
   	}
   	if(way_free != -1){
      update_entry(&l2_cache_blocks[way_free], l1_l2_entry_info->l2_tag,loadstore,-1,LRU_WB);
      update_rp_value_lru(l2_cache_blocks,l1_l2_entry_info->l2_associativity,l1_l2_entry_info->l2_associativity-1);
      if(debug){
         print_set(l2_cache_blocks,l1_l2_entry_info->l2_associativity," L2 Cache");
         print_result(*l2_result,"result : ");
      }
      
	}else{

		//REVISAR SI EL DATO ESTA EN L1 Y SI ESTA LO INVALIDO
		l1_line_invalid_set( l2_cache_blocks[candidate_vic].tag>>1, l1_l2_entry_info->l1_associativity, l1_cache_blocks, false);

		l2_result->evicted_address = l2_cache_blocks[candidate_vic].tag;
		l2_result->dirty_eviction  = l2_cache_blocks[candidate_vic].dirty;
		if(debug){ 
			print_entry(l2_cache_blocks[candidate_vic],"victimized entry: ");
			printf("\n");
		}
		int threshold = l2_cache_blocks[candidate_vic].rp_value;
		update_entry(&l2_cache_blocks[candidate_vic],l1_l2_entry_info->l2_tag,loadstore,-1,LRU_WB);
		update_rp_value_lru(l2_cache_blocks,l1_l2_entry_info->l2_associativity,threshold);
		if(debug){
			print_set(l2_cache_blocks,l1_l2_entry_info->l2_associativity,"L2 CACHE");
			print_result(*l2_result,"result : ");
		}
   }

	return OK;   
}
