/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>
#include <Victimcache.h>

typedef unsigned int u_int;
#define KB 1024
#define ADDRSIZE 32
#define BUFFER_SIZE  400
#define RP_VICTIMIZATION_VALUE 3
#define VC_ASOC 16
using namespace std;


void update_rp_value_lru(entry* cache_block, int associativity, int threshold){
   for(size_t i = 0; i < associativity; i++){
      if(cache_block[i].rp_value < threshold){
         //cout<< " antes " <<cache_block[i].rp_value <<endl;
         cache_block[i].rp_value++;
         //cout<<"despues " <<cache_block[i].rp_value <<endl;
      }
   }
}


void update_entry(entry* entry, int tag,int loadstore, int rp_value, int L1_L2){
   if(loadstore == STORE && L1_L2==2){
      entry->dirty = true;
   }else{
      if(entry->tag != tag){
         entry->dirty = false;
      }
   }
   entry->valid    = true;
   entry->tag      = tag;
   entry->rp_value = rp_value; 
}

void update_stats(stats* stats,operation_result* results_L1, operation_result* results_L2, int opt){
   
   if((results_L1->miss_hit==MISS_LOAD ||results_L1->miss_hit==MISS_STORE) && (results_L2->miss_hit==MISS_LOAD ||results_L2->miss_hit==MISS_STORE))
      stats->global_misses++;
   switch (results_L1->miss_hit)
   {
   case MISS_LOAD:
      stats->misses_L1++;
      break;
   case MISS_STORE:
      stats->misses_L1++;
      break;
   case HIT_LOAD:
      stats->hit_L1++;
      break;
   case HIT_STORE:
      stats->hit_L1++;
      break;
   default:
      break;
   }

   
   switch (opt)
   {
      case CACHE_LEVELS:
         
         switch (results_L2->miss_hit)
         {
         case MISS_LOAD:
            stats->load_misses_L2++;
            break;
         case MISS_STORE:
            stats->store_misses_L2++;
            break;
         case HIT_LOAD:
            stats->load_hit_L2++;
            break;
         case HIT_STORE:
            stats->store_hit_L2++;
            break;
         default:
            break;
         }
      break;


      case VICTIM_CACHE:
         switch (results_L2->miss_hit)
         {
         case MISS_LOAD:
            stats->misses_vc++;
            break;
         case MISS_STORE:
            stats->misses_vc++;
            break;
         case HIT_LOAD:
            stats->hits_vc++;
            break;
         case HIT_STORE:
            stats->hits_vc++;
            break;
         default:
            break;
         }
      break;
      default:
      break;
   }
    
   if(results_L2->dirty_eviction){
      stats->dirty_evictions_L2_vc++;
   }

}

//ACTUALIZAR
void print_stats(stats stats, configuration conf){
   /*Overall miss rate: 0.00
L1 miss rate: 0.00
L2 miss rate: 0.00
Global miss rate 0.00
Misses (L1): 00000
Hits (L1): 00000
Misses (L2): 00000
Hits (L2): 00000
Dirty evictions (L2): 00000*/
   int total_miss_L2=stats.load_misses_L2 + stats.store_misses_L2;
   int total_hit_L2=stats.load_hit_L2 + stats.store_hit_L2;
   double global_miss_rate= (double)stats.global_misses/((double)stats.misses_L1+(double)stats.hit_L1);
   double miss_rate_l1_vc = ((double)stats.misses_vc/(double)(stats.hit_L1+stats.misses_vc));

   printf("############################################\n");
   printf("  Simulation Results\n");
   printf("############################################\n");
   switch (conf.opt_policy){
      case CACHE_LEVELS:    
         printf(" Overall miss rate1:\t\t %f\n",global_miss_rate);
         printf(" L1 miss rate:\t\t\t %f\n",(double)stats.misses_L1/((double)stats.misses_L1+(double)stats.hit_L1));
         printf(" L2 miss rate:\t\t\t %f\n",(double)total_miss_L2/((double)total_hit_L2+(double)total_miss_L2));
         printf(" Misses (L1):\t\t\t %i\n",stats.misses_L1);
         printf(" Hits (L1):\t\t\t %i\n",stats.hit_L1);
         printf(" Misses (L2):\t\t\t %i\n",total_miss_L2);
         printf(" Hits (L2):\t\t\t %i\n",total_hit_L2);
         printf(" Dirty evictions (L2): \t\t %i\n",stats.dirty_evictions_L2_vc);
  
         break;

      case VICTIM_CACHE:
         printf(" Miss rate (L1+VC):\t\t %f\n",miss_rate_l1_vc);
         printf(" Misses (L1+VC):\t\t %i\n",(stats.misses_vc));
         printf(" Hits (L1+VC):\t\t\t %i\n",(stats.hit_L1 + stats.hits_vc));
         printf(" Victim cache hits:\t\t %i\n",stats.hits_vc);
         printf(" Dirty evictions:\t\t %i\n",stats.dirty_evictions_L2_vc);
         printf(" Hits con prefetch en L1: \t %i\n", stats.hits_l1_prefetch);
         printf(" Hits con prefetch en VC: \t %i\n", stats.hits_vc_prefetch);
            
         break;
      default: break;

   }

   printf("############################################\n");
}

void print_configuration(configuration conf){
   printf("#############################################\n");
   printf("  Cache parameters\n");
   printf("#############################################\n");
   printf(" L1 Cache size (kB):\t\t %i\n",conf.cache_size_L1);

   switch (conf.opt_policy){
      case CACHE_LEVELS:    
         
         printf(" L2 Cache size (kB):\t\t %i\n",conf.cache_size_L2);
         printf(" Cache L1 associativity:\t %i\n",conf.associativity_L1);
         printf(" Cache L2 associativity:\t %i\n",conf.associativity_L2);
         printf(" Cache block size (bytes):\t %i\n",conf.block_size);
         break;

      case VICTIM_CACHE:    
         printf(" Cache L1 associativity:\t %i\n",conf.associativity_L1);
         printf(" Cache block size (bytes):\t %i\n",conf.block_size);
         break;
      default: break;
   }

   
}


void print_entry(entry set,const char* msj){
   printf("%s{t:%i,d:%i,rp:%i,v:%i} ",msj,set.tag,set.dirty,set.rp_value,set.valid);
}

void print_set(entry* set, int assoc, const char* msj){
   printf("%s",msj);
   for(size_t i = 0; i < assoc; i++){
      print_entry(set[i],"");
   }
   printf("\n");
}

void print_result(operation_result results,const char* msj=""){
   printf("%s{%i,%i,%i}\n",msj,results.miss_hit,results.dirty_eviction,results.evicted_address);
}

int log_2(int num){
   int res = 0;
   int div = num;
   while(div != 1){
    div = div >> 1;
    res ++;
   }
   return res;
}

void process_line(char* line,long* address,int* loadstore){
	char buffer[400];
   strcpy(buffer, line);
	int cont = 0;
	char *token = strtok(buffer," ");
	while(token != NULL){
	   if(cont == 1){
	   	*loadstore = atoi(token);								
	   }
	   if(cont == 2){
	   	*address   = strtol(token,NULL,16);
	   }
	   token = strtok(NULL," ");
	   cont ++;
	}		
}
//(ACTUALIZAR)
stats simulate(FILE* file,configuration conf){ 
   long prefetch_address;
   long address;
   stats sim_stats = {};
   stats sim_stats_prefetch = {};
   char line[BUFFER_SIZE];
   address_data address_data_prefetch={};
   address_data address_data={};

   int loadstore;
   field_size_get(conf.cache_size_L1,conf.associativity_L1,conf.block_size,&address_data);

   entry  cache_blocks_L1[1<<address_data.idx_size1][conf.associativity_L1] = {};
   entry  cache_blocks_L2[1<<address_data.idx_size2][conf.associativity_L2] = {};
   entry vc_cache_blocks[VC_ASOC] ={};
   while(fgets(line, BUFFER_SIZE, file) != NULL){

      process_line(line, &address, &loadstore);




      /*Para prefetch*/
      prefetch_address=address+conf.block_size;                               //Se calcula la direccion del siguiente bloque para ser traida a cache




      address_tag_idx_get(address, &address_data);
      address_tag_idx_get(prefetch_address, &address_data_prefetch);          //Se descompone la direccion tanto del bloque i como el i+1 en idx, tag ....
      
      operation_result results_L1 = {};
      operation_result results_L2 = {};
      operation_result results_vc = {};

      operation_result results_L1_prefetch = {};                              //Se crean una nuevas estructuras de resultados para el prefetch para evitar sobre escribir datos 
      operation_result results_vc_prefetch = {};

      l1_l2_entry_info l1_l2_entry_info = {};
      l1_l2_entry_info.l1_tag=address_data.tag1;
      l1_l2_entry_info.l2_tag=address_data.tag2;
      l1_l2_entry_info.l1_associativity=conf.associativity_L1;
      l1_l2_entry_info.l2_associativity=conf.associativity_L2;
      



      struct l1_vc_entry_info l1_vc_info = {};
      l1_vc_info.l1_tag = address_data.tag1;
      l1_vc_info.l1_idx= address_data.idx1;
      l1_vc_info.l1_associativity =conf.associativity_L1;
      l1_vc_info.vc_associativity = VC_ASOC;
      


      /*La optomizacion de prefetch se realizo para VICTIM CACHE*/



      switch (conf.opt_policy)
      {  
         
         case CACHE_LEVELS:
            lru_replacement_policy_l1_l2( &l1_l2_entry_info,
                                          loadstore,
                                          cache_blocks_L1[address_data.idx1],
                                          cache_blocks_L2[address_data.idx2],
                                          &results_L1,
                                          &results_L2,
                                          false);  
            update_stats(&sim_stats,&results_L1,&results_L2,conf.opt_policy);                                    
            break;


         case VICTIM_CACHE:
            lru_replacement_policy_l1_vc(&l1_vc_info,
                                             loadstore,
                                             cache_blocks_L1[address_data.idx1],
                                             vc_cache_blocks,
                                             &results_L1,
                                             &results_vc,
                                             false);
            /*Checkeamos si hubo un miss*/
            if(results_vc.miss_hit==MISS_LOAD || results_vc.miss_hit==MISS_LOAD)
            {
               /*Debido a que hubo miss se inserta el bloque i+1 en cache como se de otra iteracion se tratase con el idx y tag correspondientes a i+1*/
               l1_vc_info.l1_tag = address_data_prefetch.tag1;
               l1_vc_info.l1_idx= address_data_prefetch.idx1;

               lru_replacement_policy_l1_vc(&l1_vc_info,
                                             loadstore,
                                             cache_blocks_L1[address_data_prefetch.idx1],
                                             vc_cache_blocks,
                                             &results_L1_prefetch,
                                             &results_vc_prefetch,
                                             false);

               /*      Con la finalidad de llevar un registro distintivo para los hits que ocaciona el prefetch       */
               /*      Se revisa si el bloque i+1 causa un hit y se incrementan los contadores creados con ese proposito   */

               if(results_L1_prefetch.miss_hit==HIT_LOAD ||results_L1_prefetch.miss_hit==HIT_STORE)
                  sim_stats.hits_l1_prefetch++;
               if(results_vc_prefetch.miss_hit==HIT_LOAD ||results_vc_prefetch.miss_hit==HIT_STORE)
                  sim_stats.hits_vc_prefetch++;

            }
            /*Se actualizan los stats*/
            update_stats(&sim_stats,&results_L1,&results_vc,conf.opt_policy);

            break;
         default:
            break;
      }
      
   }
   fclose(file);  
   return sim_stats;
}

int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    address_data* address_data)
{
    int cachesize_kb2 = 4*cachesize_kb;
    int associativity2 = 2*associativity;
   address_data->offset_size1 = log_2(blocksize_bytes);
   address_data->idx_size1    = log_2((cachesize_kb*(KB))/(associativity*blocksize_bytes));
   address_data->tag_size1    = ADDRSIZE - (address_data->idx_size1 + address_data->offset_size1);


   address_data->offset_size2 = log_2(blocksize_bytes);
   address_data->idx_size2    = log_2((cachesize_kb2*(KB))/(associativity2*blocksize_bytes));
   address_data->tag_size2    = ADDRSIZE - (address_data->idx_size2 + address_data->offset_size2);

  
   return OK;
}

void address_tag_idx_get(long address,
                         address_data* address_data)
{
    

   address_data->idx1 = (address >> address_data->offset_size1) & ((1<<address_data->idx_size1)-1);
   address_data->tag1 =  address >> (address_data->idx_size1 + address_data->offset_size1);

   address_data->idx2 = (address >> address_data->offset_size2) & ((1<<address_data->idx_size2)-1);
   address_data->tag2 =  address >> (address_data->idx_size2 + address_data->offset_size2);

   
}



int l1_line_invalid_set(int tag,
                        int associativity,
                        entry* l1_cache_blocks,
                        bool debug)

{
   int rp_invalidated=-1;
   for(size_t i = 0; i < associativity; i++){
         
			if(l1_cache_blocks[i].valid == 1){
				if(l1_cache_blocks[i].tag ==tag){
               rp_invalidated=l1_cache_blocks[i].rp_value;
					l1_cache_blocks[i].valid    = false;
               if(debug){ 
                  print_entry(l1_cache_blocks[i],"Invalidated entry: ");
                  printf("\n");
               }	
				}
         }
   }
   if(rp_invalidated!=-1){
      for(size_t i = 0; i < associativity; i++){
            
            if(l1_cache_blocks[i].rp_value>rp_invalidated){
               l1_cache_blocks[i].rp_value--;
            }
      }
   }
   
		
   return 0;
   
}
