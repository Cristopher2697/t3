/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

using namespace std;

class L2cache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST3: 
 * 1. Choose a random associativity for L1 and set associativity L2=2*asoc_L1
 * 2. Fill l1 and l2 cache entries
 * 3. Force a hit on L1 and hit on l2
 * 4. Check  miss_hit_status for both
 */
TEST_F(L2cache,l1_hit_l2_hit){
	int status = OK;
	struct l1_l2_entry_info l1_l2_entry_info;
	enum miss_hit_status expected_miss_hit_l1;
	enum miss_hit_status expected_miss_hit_l2;
	int i;
 	int l1_idx;
 	int l1_tag;
 	int l1_associativity;
	int l2_idx;
 	int l2_tag;
 	int l2_associativity;

	/* Fill a random cache entry */
	l1_tag = rand()%4096;
	l1_associativity = 1 << (rand()%4);	
	l2_tag = l1_tag >> 1;
	l2_associativity = 2*l1_associativity; 

	l1_l2_entry_info.l1_tag=l1_tag;
	l1_l2_entry_info.l2_tag=l2_tag;
	l1_l2_entry_info.l1_associativity=l1_associativity;
	l1_l2_entry_info.l2_associativity=l2_associativity;

	if (bool (debug_on)) {
		printf("Entry Info for L1\n tag: %d\n associativity: %d\n",
				l1_tag,
				l1_associativity);
		printf("Entry Info for L2\n tag: %d\n associativity: %d\n",
				l2_tag,
				l2_associativity);
	}

	struct entry l1_cache_blocks[l1_associativity] = {};
	struct entry l2_cache_blocks[l2_associativity] = {};



	operation_result l1_result = {};
	operation_result l2_result = {};

	DEBUG(debug_on, l1_hit_l2_hit_test);


	for (i = 0 ; i < 2; i++){

		/* Fill cache l1 */
		for ( i =  0; i < l1_associativity; i++) {
			l1_cache_blocks[i].valid = true;
			l1_cache_blocks[i].tag = rand()%4096;
			l1_cache_blocks[i].dirty = 0;
			l1_cache_blocks[i].rp_value = i;
		}
		l1_cache_blocks[0].tag=l1_tag;//Force hit
		if(bool (debug_on)) print_set(l1_cache_blocks,l1_associativity,"L1 SET");

		/* Fill cache l2 */
		for ( i =  0; i < l2_associativity; i++) {
			l2_cache_blocks[i].valid = true;
			l2_cache_blocks[i].tag = rand()%4096;
			l2_cache_blocks[i].dirty = 0;
			l2_cache_blocks[i].rp_value = i;
			
		}
		l2_cache_blocks[0].tag = l2_tag;//Force Hit

		if(bool (debug_on)) print_set(l2_cache_blocks,l2_associativity,"L2 SET");


		/* Load operation for i = 0, store for i =1 */
		bool loadstore = (bool)i;
		status = lru_replacement_policy_l1_l2(&l1_l2_entry_info, loadstore, l1_cache_blocks, l2_cache_blocks, &l1_result, &l2_result);

		expected_miss_hit_l1 = loadstore ? HIT_STORE: HIT_LOAD;
		expected_miss_hit_l2 = loadstore ? HIT_STORE: HIT_LOAD;
		EXPECT_EQ(l1_result.miss_hit, expected_miss_hit_l1);
		EXPECT_EQ(l2_result.miss_hit, expected_miss_hit_l2);
  	}




																					
	EXPECT_EQ(status, OK);
}

/*
 * TEST4: 
 * 1. Choose a random associativity for L1 and set associativity L2=2*asoc_L1
 * 2. Fill l1 and l2 cache entries
 * 3. Force a miss on L1 and hit on l2
 * 4. Check  miss_hit_status for both
 */
TEST_F(L2cache,l1_miss_l2_hit){
	int status = OK;
	struct l1_l2_entry_info l1_l2_entry_info;
	enum miss_hit_status expected_miss_hit_l1;
	enum miss_hit_status expected_miss_hit_l2;
	int i;
 	int l1_idx;
 	int l1_tag;
 	int l1_associativity;
	int l2_idx;
 	int l2_tag;
 	int l2_associativity;

	/* Fill a random cache entry */
	l1_tag = rand()%4096;
	l1_associativity = 1 << (rand()%4);	
	l2_tag = l1_tag >> 1;
	l2_associativity = 2*l1_associativity; 

	l1_l2_entry_info.l1_tag=l1_tag;
	l1_l2_entry_info.l2_tag=l2_tag;
	l1_l2_entry_info.l1_associativity=l1_associativity;
	l1_l2_entry_info.l2_associativity=l2_associativity;

	if (bool (debug_on)) {
		printf("Entry Info for L1\n tag: %d\n associativity: %d\n",
				l1_tag,
				l1_associativity);
		printf("Entry Info for L2\n tag: %d\n associativity: %d\n",
				l2_tag,
				l2_associativity);
	}

	struct entry l1_cache_blocks[l1_associativity] = {};
	struct entry l2_cache_blocks[l2_associativity] = {};



	operation_result l1_result = {};
	operation_result l2_result = {};

	DEBUG(debug_on, l1_miss_l2_hit_test);


	for (i = 0 ; i < 2; i++){

		/* Fill cache l1 */
		for ( i =  0; i < l1_associativity; i++) {
			l1_cache_blocks[i].valid = true;
			l1_cache_blocks[i].tag = rand()%4096;
			l1_cache_blocks[i].dirty = 0;
			l1_cache_blocks[i].rp_value = i;
			while (l1_cache_blocks[i].tag == l1_tag) { //Force miss
			l1_cache_blocks[i].tag = rand()%4096;
			}
		}
		if(bool (debug_on)) print_set(l1_cache_blocks,l1_associativity,"L1 SET");

		/* Fill cache l2 */
		for ( i =  0; i < l2_associativity; i++) {
			l2_cache_blocks[i].valid = true;
			l2_cache_blocks[i].tag = rand()%4096;
			l2_cache_blocks[i].dirty = 0;
			l2_cache_blocks[i].rp_value = i;
			
		}
		l2_cache_blocks[0].tag = l2_tag;//Force Hit

		if(bool (debug_on)) print_set(l2_cache_blocks,l2_associativity,"L2 SET");


		/* Load operation for i = 0, store for i =1 */
		bool loadstore = (bool)i;
		status = lru_replacement_policy_l1_l2(&l1_l2_entry_info, loadstore, l1_cache_blocks, l2_cache_blocks, &l1_result, &l2_result);

		expected_miss_hit_l1 = loadstore ? MISS_STORE: MISS_LOAD;
		expected_miss_hit_l2 = loadstore ? HIT_STORE: HIT_LOAD;
		EXPECT_EQ(l1_result.miss_hit, expected_miss_hit_l1);
		EXPECT_EQ(l2_result.miss_hit, expected_miss_hit_l2);
  	}




																					
	EXPECT_EQ(status, OK);
}

/*
 * TEST5: 
 * 1. Choose a random associativity for L1 and set associativity L2=2*asoc_L1
 * 2. Fill l1 and l2 cache entries
 * 3. Force a miss on L1 and miss on l2
 * 4. Check  miss_hit_status for both
 */
TEST_F(L2cache,l1_miss_l2_miss){
	int status = OK;
	struct l1_l2_entry_info l1_l2_entry_info;
	enum miss_hit_status expected_miss_hit_l1;
	enum miss_hit_status expected_miss_hit_l2;
	int i;
 	int l1_idx;
 	int l1_tag;
 	int l1_associativity;
	int l2_idx;
 	int l2_tag;
 	int l2_associativity;

	/* Fill a random cache entry */
	l1_tag = rand()%4096;
	l1_associativity = 1 << (rand()%4);	
	l2_tag = l1_tag >> 1;
	l2_associativity = 2*l1_associativity; 

	l1_l2_entry_info.l1_tag=l1_tag;
	l1_l2_entry_info.l2_tag=l2_tag;
	l1_l2_entry_info.l1_associativity=l1_associativity;
	l1_l2_entry_info.l2_associativity=l2_associativity;

	if (bool (debug_on)) {
		printf("Entry Info for L1\n tag: %d\n associativity: %d\n",
				l1_tag,
				l1_associativity);
		printf("Entry Info for L2\n tag: %d\n associativity: %d\n",
				l2_tag,
				l2_associativity);
	}

	struct entry l1_cache_blocks[l1_associativity] = {};
	struct entry l2_cache_blocks[l2_associativity] = {};



	operation_result l1_result = {};
	operation_result l2_result = {};

	DEBUG(debug_on, l1_miss_l2_hit_test);


	for (i = 0 ; i < 2; i++){

		/* Fill cache l1 */
		for ( i =  0; i < l1_associativity; i++) {
			l1_cache_blocks[i].valid = true;
			l1_cache_blocks[i].tag = rand()%4096;
			l1_cache_blocks[i].dirty = 0;
			l1_cache_blocks[i].rp_value = i;
			while (l1_cache_blocks[i].tag == l1_tag) { //Force miss
				l1_cache_blocks[i].tag = rand()%4096;
			}
		}
		if(bool (debug_on)) print_set(l1_cache_blocks,l1_associativity,"L1 SET");

		/* Fill cache l2 */
		for ( i =  0; i < l2_associativity; i++) {
			l2_cache_blocks[i].valid = true;
			l2_cache_blocks[i].tag = rand()%4096;
			l2_cache_blocks[i].dirty = 0;
			l2_cache_blocks[i].rp_value = i;
			while (l2_cache_blocks[i].tag == l2_tag) { //Force miss
				l2_cache_blocks[i].tag = rand()%4096;
			}
		}

		if(bool (debug_on)) print_set(l2_cache_blocks,l2_associativity,"L2 SET");


		/* Load operation for i = 0, store for i =1 */
		bool loadstore = (bool)i;
		status = lru_replacement_policy_l1_l2(&l1_l2_entry_info, loadstore, l1_cache_blocks, l2_cache_blocks, &l1_result, &l2_result);

		expected_miss_hit_l1 = loadstore ? MISS_STORE: MISS_LOAD;
		expected_miss_hit_l2 = loadstore ? MISS_STORE: MISS_LOAD;
		EXPECT_EQ(l1_result.miss_hit, expected_miss_hit_l1);
		EXPECT_EQ(l2_result.miss_hit, expected_miss_hit_l2);
  	}




																					
	EXPECT_EQ(status, OK);
}


/*
 * TEST6: 
 * 1. Choose a random associativity for L1 and set associativity L2=2*asoc_L1
 * 2. Fill l1 and l2 cache entries
 * 3. Force a miss on L1 and miss on l2
 * 4. Check  miss_hit_status for both
 */
TEST_F(L2cache,inv_l1_vict_l2){
	int status = OK;
	struct l1_l2_entry_info l1_l2_entry_info;
	enum miss_hit_status expected_miss_hit_l1;
	enum miss_hit_status expected_miss_hit_l2;
	int i;
 	int l1_idx;
 	int l1_tag;
 	int l1_associativity;
	int l2_idx;
 	int l2_tag;
 	int l2_associativity;

	/* Fill a random cache entry */
	l1_tag = rand()%4096;
	l1_associativity = 1 << (rand()%4);	
	while(l1_associativity ==1){
		l1_associativity = 1 << (rand()%4);	
	}
	l2_tag = l1_tag >> 1;
	l2_associativity = 2*l1_associativity; 

	l1_l2_entry_info.l1_tag=l1_tag;
	l1_l2_entry_info.l2_tag=l2_tag;
	l1_l2_entry_info.l1_associativity=l1_associativity;
	l1_l2_entry_info.l2_associativity=l2_associativity;

	if (bool (debug_on)) {
		printf("Entry Info for L1\n tag: %d\n associativity: %d\n",
				l1_tag,
				l1_associativity);
		printf("Entry Info for L2\n tag: %d\n associativity: %d\n",
				l2_tag,
				l2_associativity);
	}

	struct entry l1_cache_blocks[l1_associativity] = {};
	struct entry l2_cache_blocks[l2_associativity] = {};



	operation_result l1_result = {};
	operation_result l2_result = {};

	DEBUG(debug_on, l1_miss_l2_hit_test);


	for (i = 0 ; i < 2; i++){

		/* Fill cache l1 */
		for ( i =  0; i < l1_associativity; i++) {
			l1_cache_blocks[i].valid = true;
			l1_cache_blocks[i].tag = rand()%4096;
			l1_cache_blocks[i].dirty = 0;
			l1_cache_blocks[i].rp_value = i;
			while (l1_cache_blocks[i].tag == l1_tag) { //Force miss
				l1_cache_blocks[i].tag = rand()%4096;
			}
		}
		if(bool (debug_on)) print_set(l1_cache_blocks,l1_associativity,"L1 SET");

		/* Fill cache l2 */
		for ( i =  0; i < l2_associativity; i++) {
			l2_cache_blocks[i].valid = true;
			l2_cache_blocks[i].tag = rand()%4096;
			l2_cache_blocks[i].dirty = 0;
			l2_cache_blocks[i].rp_value = i;
			while (l2_cache_blocks[i].tag == l2_tag) { //Force miss
				l2_cache_blocks[i].tag = rand()%4096;
			}
		}
		//Nos aseguramos que una via de L1 contenga un dato correspondiente al mismo addres que se va a victimizar en L2
		//Para que al victimizar un dato en L2 su correspondiente dato en l1 se invalide
		l1_cache_blocks[0].tag = l2_cache_blocks[l2_associativity-1].tag >> 1;

		if(bool (debug_on)) print_set(l2_cache_blocks,l2_associativity,"L2 SET");

		/* Load operation for i = 0, store for i =1 */
		bool loadstore = (bool)i;

		status = lru_replacement_policy_l1_l2(&l1_l2_entry_info, loadstore, l1_cache_blocks, l2_cache_blocks, &l1_result, &l2_result);

		expected_miss_hit_l1 = loadstore ? MISS_STORE: MISS_LOAD;
		expected_miss_hit_l2 = loadstore ? MISS_STORE: MISS_LOAD;
		EXPECT_EQ(l1_result.miss_hit, expected_miss_hit_l1);
		EXPECT_EQ(l2_result.miss_hit, expected_miss_hit_l2);
		EXPECT_EQ(l1_cache_blocks[0].valid, false);
  	}




																					
	EXPECT_EQ(status, OK);
}
