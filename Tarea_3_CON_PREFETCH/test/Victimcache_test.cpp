/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>


#define VC_ASOC 16
using namespace std;

class VCcache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST1: victim cache hit
 * 1. Choose a random associativity for L1
 * 2. Fill l1 and vc cache entries
 * 3. Force a miss on L1 and a hit on VC
 * 4. Check  miss_hit_status for VC
 */
TEST_F(VCcache,l1_miss_vc_hit){
	//srand (time(NULL)); 
	int status = OK;
	int i,k;
  	int idx;
  	int tag;
	
	bool match = false;
	int associativity_l1;
	

	

	enum miss_hit_status expected_miss_hit;
	struct l1_vc_entry_info l1_vc_info;
   operation_result l1_result = {};
   operation_result vc_result = {};
	/*Fill a random cache entry*/
	idx = rand()%1024;
	tag = rand()%4096;
	associativity_l1 = 1 << (rand()%4);
	struct entry l1_cache_blocks[associativity_l1] = {};
	struct entry vc_cache_blocks[VC_ASOC] = {};
	l1_vc_info.l1_tag=tag;
	l1_vc_info.l1_associativity=associativity_l1;
	l1_vc_info.vc_associativity=VC_ASOC;
if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity_l1);

  }

	DEBUG(debug_on, l1_miss_vc_hit_test);
	for (i = 0 ; i < 2; i++)
	{
    /* Fill l1 */
		for ( i =  0; i < associativity_l1; i++) {
			
			l1_cache_blocks[i].valid = true;
			l1_cache_blocks[i].tag = rand()%4096;
			l1_cache_blocks[i].dirty = 0;
			l1_cache_blocks[i].rp_value = i;
			//forzar miss en l1
			while (l1_cache_blocks[i].tag == tag) {
				l1_cache_blocks[i].tag = rand()%4096;
			}
			
		}
		/*fill vc*/
		for(k =0; k <VC_ASOC; k++)
		{
			
			
			vc_cache_blocks[k].valid = true;
			vc_cache_blocks[k].tag = rand()%4096;	
			vc_cache_blocks[k].dirty = 0;
			vc_cache_blocks[k].rp_value = k;
			
			
		}
			//Forzar un hit en vc
			vc_cache_blocks[0].tag = tag;
		
		if(bool (debug_on))
		{
			cout << "INFO" <<endl;
		 	print_set(l1_cache_blocks,associativity_l1,""); 
			print_set(vc_cache_blocks,VC_ASOC,""); 
		}
		bool loadstore = (bool)i;
		match = false;
  		status = lru_replacement_policy_l1_vc(&l1_vc_info,
  		    	                        		loadstore,
  		      	                      	      	l1_cache_blocks,
  		        	                    	    vc_cache_blocks,
  		          	                  	      	&l1_result,
  		            	                	    &vc_result, false);

		EXPECT_EQ(status, OK);
		EXPECT_EQ(vc_result.dirty_eviction, 0);
    	expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    	EXPECT_EQ(vc_result.miss_hit, expected_miss_hit);

	}


  
}



/*
 * TEST2: victim cache miss
 * 1. Choose a random associativity for L1
 * 2. Fill l1 and vc cache entries
 * 3. Force a miss on L1 and a miss on VC
 * 4. Check  miss_hit_status for VC
 */
TEST_F(VCcache,l1_miss_vc_miss){
	
	int status = OK;
	int i,k;
  	int idx;
  	int tag;
	bool match = false;
	int associativity_l1;
	enum miss_hit_status expected_miss_hit;
	struct l1_vc_entry_info l1_vc_info;
   	operation_result l1_result = {};
   	operation_result vc_result = {};
	/*Fill a random cache entry*/
	idx = rand()%1024;
	tag = rand()%4096;
	associativity_l1 = 1 << (rand()%4);
	struct entry l1_cache_blocks[associativity_l1] = {};
	struct entry vc_cache_blocks[VC_ASOC] = {};
	l1_vc_info.l1_tag=tag;
	l1_vc_info.l1_associativity=associativity_l1;
	l1_vc_info.vc_associativity=VC_ASOC;
if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity_l1);
  }

	DEBUG(debug_on, l1_miss_vc_miss_test);
	for (i = 0 ; i < 2; i++)
	{
    /* Fill l1 */
		for ( i =  0; i < associativity_l1; i++) 
		{
				
				l1_cache_blocks[i].valid = true;
				l1_cache_blocks[i].tag = rand()%4096;
				l1_cache_blocks[i].dirty = 0;
				l1_cache_blocks[i].rp_value = i;
				//forzar miss en l1
				while (l1_cache_blocks[i].tag == tag) 
				{
					l1_cache_blocks[i].tag = rand()%4096;
				}
		}
		/*fill vc*/
		for(k =0; k <VC_ASOC; k++)
		{
			vc_cache_blocks[k].valid = true;
			//Forzar miss en vc
			while( vc_cache_blocks[k].tag == tag)
			{
				vc_cache_blocks[k].tag = rand()%4096;
			}
			vc_cache_blocks[k].dirty = 0;
			vc_cache_blocks[k].rp_value = k;
		}

			bool loadstore = (bool)i;	
			if(bool (debug_on))
			{
				cout << "INFO" <<endl;
		 		print_set(l1_cache_blocks,associativity_l1,""); 
				print_set(vc_cache_blocks,VC_ASOC,""); 
			}
  			status = lru_replacement_policy_l1_vc(&l1_vc_info,
  			    	                        		loadstore,
  			      	                      	      	l1_cache_blocks,
  			        	                    	    vc_cache_blocks,
  			          	                  	      	&l1_result,
  			            	                	    &vc_result, false);

			EXPECT_EQ(status, OK);
			EXPECT_EQ(vc_result.dirty_eviction, 0);
    		expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    		EXPECT_EQ(vc_result.miss_hit, expected_miss_hit);
	} 
}
