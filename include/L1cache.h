/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#ifndef L1CACHE_H
#define L1CACHE_H

#include <netinet/in.h> 
/* 
 * ENUMERATIONS 
 */

/* Return Values */
enum returns_types {
 OK,
 PARAM,
 ERROR
};

/* Represent the cache replacement policy */
enum replacement_policy{
 LRU,
 NRU,
 RRIPHP,
 RRIPFP,
 RANDOM 
};


enum opt_policy{
CACHE_LEVELS,
VICTIM_CACHE

};

enum miss_hit_status {
 MISS_LOAD,
 MISS_STORE,
 HIT_LOAD,
 HIT_STORE
};

enum load_store{
    LOAD,
    STORE
};

/*
 * STRUCTS
 */

/* Cache tag array fields */
struct entry {
    bool valid;
    bool dirty;
    int  tag;
    int8_t rp_value; // cambio
};

/* Cache replacement policy results */
typedef struct{
    enum miss_hit_status miss_hit;
    bool dirty_eviction;
    int  evicted_address;
}operation_result;

typedef struct{
    int cache_size_L1;
    int cache_size_L2;
    int block_size;
    int associativity_L1;
    int associativity_L2;
    int opt_policy;
}configuration;

typedef struct{
    int cache_size;
    int associtivity;
    int block_size;
    int dirty_evictions_L2_vc;
    int load_misses_L2;
    int store_misses_L2;
    int load_hit_L2;
    int store_hit_L2;
    int misses_L1;
    int hit_L1;
    int global_misses;
    ///vc
    int misses_vc;
    int hits_vc;
    

}stats;


typedef struct{
    int tag_size1;
    int idx_size1;
    int offset_size1;
    int tag_size2;
    int idx_size2;
    int offset_size2;

    int tag1;
    int idx1;
    int offset1;
    int tag2;
    int idx2;
    int offset2;

}address_data;


/* 
 *  Functions
 */


void update_rp_value_lru(entry* cache_block, int associativity, int threshold);

/*

*  Update cache entry values when a block is replaced
* [in] entry: reference to an entry of cache
* [in] tag: new value of tag
* [in] loadsrore: identifier for a type of entry (load or store) needed to update the dirty bit
* [in] rp_value: New value of the rp number
*/
void update_entry(entry* entry, int tag, int loadstore, int rp_value, int entrL2);
/**
 * Update results of dirty bit, evicted tag and miss-hit counters
 * [in] stats: reference to struct of stats 
 * [in] results: reference to struct of results
 **/
void update_stats(stats* stats,operation_result* results_L1,operation_result* results_L2, int opt);


/**
 * Print a summary of inputs to simulate
 * [in] conf: struct with input configuration info
 **/

void print_configuration(configuration conf);

void print_result(operation_result results,const char* msj);

/**
 * Print a summary of results
 * [in] stats: struct with results info
 **/
void print_stats(stats stats, configuration conf);

void print_entry(entry set,const char* msj);

/**
 * Print an especific set of cache
 * [in] set: reference to the first position of the set
 * [in] assoc: associativity, number of ways of the set
 * [in] msj: empty string to concatenate entry info into a string array 
 **/
void print_set(entry* set, int assoc, const char* msj);

/* 
 * Get an log_2 of a number with multiplicity of 2
 * 
 * [in] num: numer
 * 
 * 
 * [out] result: result of the log_2
 *

 */
int log_2(int num);

/* 
 * Get address from input line
 * 
 * [in] line: input line from trace file
 * [in] address: full address
 * [in] loadstore: indicate if this acces its load or store from input
 *

 */

void process_line(char* line,long* address,int* loadstore);

/**
 * Simulate cache update process
 * [in] file: compress file with instructions info
 * [in] conf: struct with input configuration info
 * [out] sim_stats: struct of stats with final results
 **/

stats simulate(FILE* file,configuration conf);

/*
 * Get tag, index and offset length
 * 
 * [in] cache_size: total size of the cache in Kbytes
 * [in] associativity: number of ways of the cache
 * [in] blocksize_bytes: size of each cache block in bytes
 *
 * [out] tag_size: size in bits of the tag field
 * [out] idx_size: size in bits of the index field
 * [out] offset_size: size in bits of the offset size
 */
int field_size_get(int cachesize_kb,
                   int associativity,
                   int blocksize_bytes,
                   address_data* address_data);

/* 
 * Get tag and index from address
 * 
 * [in] address: memory address
 * [in] tag_size: number of bits of the tag field
 * [in] idx_size: number of bits of the index field
 *
 * [out] idx: cache line idx
 * [out] tag: cache line tag
 */

void address_tag_idx_get(long address,
                         address_data* address_data);



/* 
 * Search for an address in a cache set and
 * replaces blocks using LRU policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */

/* 
 * Invalidates a line in the cache 
 * 
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: cache entry to edit
 *
 * return error if tag is not found in cache blocks
 */
int l1_line_invalid_set(int tag,
                        int associativity,
                        entry* cache_blocks,
                        bool debug=false);

#endif
